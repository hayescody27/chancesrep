import java.util.Scanner;
   public class ProjectedSales
      {
         public static void main(String[] args)
            {
               Scanner in = new Scanner(System.in);
               double incRate = 0.10;
               System.out.println("Enter sales total for North:   ");
               double northS = in.nextDouble();
               System.out.println("Enter sales total for South:    ");
               double southS = in.nextDouble(); 
               double nextN = (northS*incRate)+northS;
               double nextS = (southS+incRate)+southS;
               System.out.printf("Next years projected sales for North division is %.2f and Souths sales are %.2f.",nextN, nextS);
                
            }
      }