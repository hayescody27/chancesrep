package application;

public class ProcessCalc {
	private int daysKept = 0;
	private double weightDog = 0;
	private double priceTotal = 0;
	
	public void calculate(double weightDog, int daysKept) {
		priceTotal = .5 * weightDog * daysKept;
	}
	
	public int getDaysKept() {
		return daysKept;
	}
	public void setDaysKept(int daysKept) {
		this.daysKept = daysKept;
	}
	public double getWeightDog() {
		return weightDog;
	}
	public void setWeightDog(double weightDog) {
		this.weightDog = weightDog;
	}
	public double getPriceTotal() {
		return priceTotal;
	}
	public void setPriceTotal(double priceTotal) {
		this.priceTotal = priceTotal;
	}
	
}
