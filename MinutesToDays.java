import java.util.Scanner;
   public class MinutesToDays
      {
         public static void main(String[] args)
            {
               Scanner in = new Scanner(System.in);
               System.out.println("Enter number of minutes:  ");
               double minutes = in.nextDouble();
               double minToHours = minutes/60;
               double hoursToDays = minToHours/24;
               System.out.printf(" %.2f minutes is %.2f hours and %.2f days.",minutes, minToHours, hoursToDays);
            }
      }