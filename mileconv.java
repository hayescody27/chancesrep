import java.util.Scanner;
   public class mileconv
   {
      public static void main(String[] args)
      {
         Scanner in = new Scanner(System.in);
         int ftInmile = 5280;
         System.out.println("Enter number of miles:  ");
         double miles = in.nextDouble();
         double feet = miles*ftInmile;
         System.out.printf("%.2f miles is %.2f feet.", miles,feet);
      }
   }