public class TipCalc //Shows the tip ammount and total
{
   public static void main(String[] args)
   {
      double bill = 14.70; 
      int tipRate = 20;
      double tipTotal = (tipRate/100.0) * bill;
      double totalBill = (tipRate/100.0) * bill + bill;
      System.out.println("A " + tipRate + " percent tip on a bill of " + bill + " is " + tipTotal + "." + " The total bill is " + totalBill);
   }
}